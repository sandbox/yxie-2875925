<?php
namespace Drupal\points\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

class PointTypeForm extends EntityForm {
  /**
   * The point type storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storage;

  /**
   * Creates a new PointTypeForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The point type storage.
   */
  public function  __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->storage = $entity_type_manager->getStorage('point_type');

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = $container->get('entity_type.manager');

    return new static($entity_type_manager);
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $point_type = $this->entity;
    
    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Type name'),
      '#maxlength' => 255,
      '#default_value' => $point_type->getName(),
      '#required' => TRUE,
    );
    /*$form['id'] = array(
      '#type' => 'machine_name',
      '#default_value' => $point_type->id(),
      '#machine_name' => array(
        'exists' => array($this, 'exist'),
      ),
      '#disabled' => !$point_type->isNew(),
    );*/
    $form['type'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Point type'),
      '#default_value' => $point_type->getType(),
      '#element_validate' => ['::validatePointType'],
      '#maxlength' => 30,
      '#placeholder' => 'pointType',
      '#disabled' => !$point_type->isNew(),
      '#required' => TRUE,
    ];

    return $form;
  }


  /**
   * Validates the point type.
   */
  public function validatePointType(array $element, FormStateInterface $form_state, array $form) {
    $point_type_entity = $this->getEntity();
    $point_type = $element['#value'];
    if ($point_type_entity->isNew()) {
      $loaded_type = $this->storage->load($point_type);
      if ($loaded_type) {
        $form_state->setError($element, $this->t('The point type is already in use.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $point_type_entity = $this->entity;
    $status = $point_type_entity->save();
    if ($status) {
      drupal_set_message($this->t('Saved the %label type.', [
        '%label' => $point_type_entity->label(),
      ]));
    }
    else {
      drupal_set_message($this->t('The %label point type was not saved.', [
        '%label' => $point_type_entity->label(),
      ]));
    }

    $form_state->setRedirect('entity.point_type.collection');
  }

}