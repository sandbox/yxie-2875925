<?php
namespace  Drupal\points\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an point_type entity.
 */
interface PointTypeInterface extends ConfigEntityInterface {
  /**
   * Sets the point type
   *
   * @param string $type
   *   point type.
   *
   * @return $this
   */
  public function setType($type);

  /**
   * Sets the point type name.
   *
   * @param string $name
   *   The type name.
   *
   * @return $this
   */
  public function setName($name);

/*  public function getType();

  public function getName();*/


}