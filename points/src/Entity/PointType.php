<?php

namespace Drupal\points\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\points\Entity\PointTypeInterface;

/**
 * Defines the point_type entity.
 *
 * @ConfigEntityType(
 *   id = "point_type",
 *   label = @Translation("Point Type"),
 *   handlers = {
 *     "list_builder" = "Drupal\points\Controller\PointTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\points\Form\PointTypeForm",
 *       "edit" = "Drupal\points\Form\PointTypeForm",
 *       "delete" = "Drupal\points\Form\PointTypeDeleteForm",
 *     }
 *   },
 *   config_prefix = "point_type",
 *   admin_permission = "administer point_type",
 *   entity_keys = {
 *     "id" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "edit-form" = "/admin/point/config/point_type/{point_type}",
 *     "delete-form" = "/admin/point/config/point_type/{point_type}/delete",
 *   }
 * )
 */
class PointType  extends  ConfigEntityBase implements PointTypeInterface {

  /**
   * The  point type.
   *
   * @var string
   */
  protected $type;

  /**
   * The point type name.
   *
   * @var string
   */
  protected $name;

  /**
   * Overrides \Drupal\Core\Entity\Entity::id().
   */
  public function id() {
    return $this->type;
  }

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return $this->type;
  }

  /**
   * {@inheritdoc}
   */
  public function setType($type) {
    $this->type = $type;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->name;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->name = $name;
    return $this;
  }
}