<?php

namespace Drupal\points\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\FormElement;
use Masterminds\HTML5\Exception;

/**
 * Provides a price form element.
 *
 * Usage example:
 * @code
 * $form['point'] = [
 *   '#type' => 'point_type',
 *   '#title' => $this->t('point_type'),
 *   '#default_value' => ['number' => '10', 'point_type' => 'balance'],
 *   '#allow_negative' => FALSE,
 *   '#size' => 60,
 *   '#maxlength' => 128,
 *   '#required' => TRUE,
 *   '#available_point_type' => ['balance', 'integral'],
 * ];
 * @endcode
 *
 * @FormElement("point_type")
 */
class Type extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      // List of point type.
      '#available_point_type' => [],

      '#size' => 10,
      '#maxlength' => 128,
      '#default_value' => NULL,
      '#allow_negative' => FALSE,
      '#attached' => [
        'library' => ['points/admin'],
      ],
      '#process' => [
        [$class, 'processElement'],
        [$class, 'processAjaxForm'],
        [$class, 'processGroup'],
      ],
      '#pre_render' => [
        [$class, 'preRenderGroup'],
      ],
      '#input' => TRUE,
      '#theme_wrappers' => ['container'],
    ];
  }

  /**
   * Builds the point_type form element.
   *
   * @param array $element
   *   The initial point_type form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The built point_type form element.
   *
   */
  public static function processElement(array $element, FormStateInterface $form_state, array &$complete_form) {
    $default_value = $element['#default_value'];
    //todo : validate
    if (isset($default_value) && !self::validateDefaultValue($default_value)) {
        throw new Exception('The #default_value for a points element must be an array with "number" and "point_type" keys.');
    }

    $element['#tree'] = TRUE;
    $element['#attributes']['class'][] = 'form-type-points-type';

    $element['number'] = [
      '#type' => 'point_number',
      '#title' => $element['#title'],
      '#default_value' => $default_value ? $default_value['number'] : NULL,
      '#required' => $element['#required'],
    ];

    $point_type = ['integral'=> '积分', 'balance' => '余额'];

    $element['point_type'] = [
        '#type' => 'select',
        '#title' => t(' point type'),
        '#default_value' => $default_value ? $default_value['point_type'] : NULL,
        '#options' => $point_type,
        '#title_display' => 'invisible',
        '#field_suffix' => '',
      ];

    return $element;
  }

  /**
   * Validates the default value.
   *
   * @param mixed $default_value
   *   The default value.
   *
   * @return bool
   *   TRUE if the default value is valid, FALSE otherwise.
   */
  public static function validateDefaultValue($default_value) {
    if (!is_array($default_value)) {
      return FALSE;
    }
    if (!array_key_exists('number', $default_value) || !array_key_exists('point_type', $default_value)) {
      return FALSE;
    }
    return TRUE;
  }

}
