<?php

namespace Drupal\Points\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'points_default' widget.
 *
 * @FieldWidget(
 *   id = "points_default",
 *   label = @Translation("Points"),
 *   field_types = {
 *     "points"
 *   }
 * )
 */
class PointsDefaultWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['#type'] = 'point_type';
    if (!$items[$delta]->isEmpty()) {
      // todo:
//      dpm($items, 'items');
    }
    $element['#available_point_type'] = $this->getFieldSetting('available_point_type');

    return $element;
  }

}
