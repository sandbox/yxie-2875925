<?php

namespace Drupal\Points\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'points_default' formatter.
 *
 * @FieldFormatter(
 *   id = "points_default",
 *   module = "Points",
 *   label = @Translation("point defualt formatter"),
 *   field_types = {
 *     "points"
 *   }
 * )
 */
class PointDefaultFormatter extends FormatterBase {
  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'display_type' => TRUE,
      ] + parent::defaultSettings();
  }


  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];
    $elements['display_type'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display the point type.'),
      '#default_value' => $this->getSetting('display_type'),
    ];

    return $elements;
  }
  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    if ($this->getSetting('display_type')) {
      $summary[] = $this->t('Display the point type.');
    }
    else {
      $summary[] = $this->t('Do not display the point type.');
    }
    return $summary;
  }
  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
//    dpm($items);
  }

}
