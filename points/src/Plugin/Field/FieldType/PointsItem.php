<?php

namespace Drupal\points\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 *
 * Plugin implementation of the Points field type.
 *
 * @FieldType(
 *   id = "points",
 *   label = @Translation("Points"),
 *   description = @Translation("This field define a points"),
 *   category = @Translation("Point"),
 *   default_formatter = "points_default",
 *   default_widget = "points_default",
 * )
 */
class PointsItem extends FieldItemBase {

  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['number'] = DataDefinition::create('string')
      ->setLabel(t('Number'))
      ->setRequired('False');

    $properties['point_type'] = DataDefinition:: create('string')
      ->setLabel(t('Point type'))
      ->setRequired('False');
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'number' => [
          'description' => 'The number.',
          'type' => 'numeric',
          'precision' => 19,
          'scale' => 6,
        ],
        'point_type' => [
          'description' => 'The point type.',
          'type' => 'varchar',
          'length' => 3,
        ],
      ],
    ];

  }
  /**
   * 覆盖FieldItemBase :: defaultFieldSettings（）以设置默认值
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    // 声明可用的point的类型available_point_type， 并给一个空的默认值
    return [
        'available_point_type' => [],
      ] + parent::defaultFieldSettings();
  }

  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {

    // todo: 设置point type 选择表单
    $point_type = ['integral'=> '积分', 'balance' => '余额'];
    $element = [];
    $element['available_point_type'] = [
      '#type' => count($point_type) < 10 ? 'checkboxes' : 'select',
      '#title' => $this->t('point type'),
      '#description' => $this->t('If no type are selected, all type will be available.'),
      '#options' => $point_type,
      '#default_value' => $this->getSetting('available_point_type'),
      '#multiple' => TRUE,
      '#size' => 5,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $number = $this->get('number')->getValue();
    $point_type = $this->get('point_type')->getValue();
    return $number === NULL || $number === '' || empty($point_type);
  }

}



